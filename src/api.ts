/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { json } from 'body-parser'
import * as express from 'express'
import { createServer as createHttpServer } from 'http'
import * as socketIo from 'socket.io'
import { Database } from './database'
import { handleSyncRequest, hashAuthToken } from './sync'

export const createServer = ({ database }: {
  database: Database
}) => {
  const app = express()
  const server = createHttpServer(app)
  const io = socketIo(server)

  app.post('/sync', json({
    limit: '5mb'
  }), async (req, res, next) => {
    try {
      const { changes, response } = await handleSyncRequest({
        request: req.body,
        database
      })

      changes.forEach((change) => {
        io.to(change.deviceIdAndAuthTokenHash).emit('changes', change.content)
      })

      res.json(response)
    } catch (ex) {
      next(ex)
    }
  })

  app.get('/time', (_, res) => {
    res.json({
      ms: Date.now()
    })
  })

  io.on('connection', (socket) => {
    socket.on('subscribe', (deviceId: any, authToken: any, listener: any) => {
      if (typeof deviceId !== 'string' || typeof authToken !== 'string' || typeof listener !== 'function') {
        if (typeof listener === 'function') {
          listener('bad request')
        }

        return
      }

      try {
        const room = deviceId + hashAuthToken(authToken)

        socket.join(room)
        listener('ok')
      } catch (ex) {
        listener('error')
      }
    })
  })

  return server
}
