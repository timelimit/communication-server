/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as Ajv from 'ajv'
import { uniq, uniqWith } from 'lodash'
import { SyncRequest } from './schema'

const ajv = new Ajv({ schemaId: 'id' })

const requestSchema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  $id: 'http://timelimit.io/communication-server.request.json',
  type: 'object',
  properties: {
    deviceId: {
      type: 'string',
      pattern: '^[a-zA-Z0-9+/=]{24}$'
    },
    authToken: {
      type: 'string',
      minLength: 32
    },
    sendMessages: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          sequenceId: {
            type: 'number',
            minimum: 0,
            multipleOf: 1
          },
          receiverDeviceId: {
            type: 'string',
            pattern: '^[a-zA-Z0-9+/=]{24}$'
          },
          content: {
            type: 'string',
            pattern: '^[a-zA-Z0-9+/=]*$'
          }
        },
        required: ['sequenceId', 'receiverDeviceId', 'content']
      }
    },
    putStatus: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          statusId: {
            type: 'string',
            pattern: '^[a-zA-Z0-9]{10}$'
          },
          receiverDeviceIds: {
            type: 'array',
            items: {
              type: 'string',
              pattern: '^[a-zA-Z0-9+/=]{24}$'
            },
            minItems: 1
          },
          newContent: {
            type: 'string',
            pattern: '^[a-zA-Z0-9+/=]*$'
          }
        },
        required: ['statusId', 'receiverDeviceIds']
      }
    },
    receivedStatus: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          senderDeviceId: {
            type: 'string',
            pattern: '^[a-zA-Z0-9+/=]{24}$'
          },
          statusId: {
            type: 'string',
            pattern: '^[a-zA-Z0-9]{10}$'
          },
          contentHash: {
            type: 'string',
            pattern: '^[0-9a-f]{128}$'
          }
        },
        required: ['senderDeviceId', 'statusId', 'contentHash']
      }
    },
    receivedMessageIds: {
      type: 'array',
      items: {
        type: 'string',
        pattern: '[a-zA-Z0-9]{10}'
      },
      uniqueItems: true
    }
  },
  required: [
    'deviceId',
    'authToken',
    'sendMessages',
    'putStatus',
    'receivedStatus',
    'receivedMessageIds'
  ]
}

const validateRequestSchema = ajv.compile(requestSchema) as (param: any) => param is SyncRequest

export function validateRequest (request: any): request is SyncRequest {
  if (!validateRequestSchema(request)) {
    return false
  }

  const allSendStatusIds = request.putStatus.map((item) => item.statusId)

  if (allSendStatusIds.length !== uniq(allSendStatusIds).length) {
    return false
  }

  if (request.putStatus.some((item) => item.receiverDeviceIds.length === 0)) {
    return false
  }

  if (request.receivedStatus.length !== uniqWith(request.receivedStatus, (a, b) => (
    a.senderDeviceId === b.senderDeviceId && a.statusId === b.statusId
  )).length) {
    return false
  }

  if (
    request.sendMessages.some((message) => message.receiverDeviceId === request.deviceId)
  ) {
    return false
  }

  if (
    request.putStatus.some((status) => (
      status.receiverDeviceIds.indexOf(request.deviceId) !== -1
    ))
  ) {
    return false
  }

  return true
}
