/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export interface SyncRequest {
  deviceId: string
  authToken: string
  sendMessages: Array<{
    sequenceId: number
    receiverDeviceId: string
    content: string
  }>
  putStatus: Array<{
    statusId: string
    receiverDeviceIds: Array<string>
    newContent?: string
  }>
  receivedStatus: Array<{
    senderDeviceId: string
    statusId: string
    contentHash: string
  }>
  receivedMessageIds: Array<string>
}

export interface SyncResponse {
  missingStatusContent: Array<string>
  pendingSentMessageSequenceIds: Array<number>
  statusUpdates: Array<{
    statusId: string
    senderDeviceId: string
    content?: string
    contentHash?: string
    time: number
  }>
  newMessages: Array<{
    messageId: string
    senderDeviceId: string
    content: string
  }>
  time: number
}

export interface ExecutedChanges {
  devicesWithNewMessages: Array<string>
  devicesWithNewStatusContent: Array<string>
  devicesWithRemovedSentMessages: Array<string>
}

export type ChangeNotifications = Array<ChangeNotification>

export interface ChangeNotification {
  deviceIdAndAuthTokenHash: string
  content: {
    messages: boolean
    status: boolean
    deliveredMessages: boolean
  }
}
