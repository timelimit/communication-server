/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Op } from 'sequelize'
import { Database } from './database'

export async function cleanupDatabase (database: Database) {
  const now = Date.now()
  const oneWeekAgo = now - 1000 * 60 * 60 * 24 * 7
  const fourWeeksAgo = now - 1000 * 60 * 60 * 24 * 7 * 4

  await database.transaction(async (transaction) => {
    await database.device.destroy({
      where: {
        lastSeen: {
          [Op.lt]: fourWeeksAgo
        }
      },
      transaction
    })

    await database.message.destroy({
      where: {
        time: {
          [Op.lt]: oneWeekAgo
        }
      },
      transaction
    })
  })

  // returns true if it should be called again
  async function cleanupStatusRound (): Promise<boolean> {
    return database.transaction(async (transaction) => {
      const oldStatusItems = await database.status.findAll({
        where: {
          time: {
            [Op.lt]: oneWeekAgo
          }
        },
        attributes: ['senderDeviceId', 'statusId'],
        transaction
      }).map((item) => ({
        senderDeviceId: item.senderDeviceId,
        statusId: item.statusId
      }))

      if (oldStatusItems.length === 0) {
        return false
      }

      await database.statusReceiver.destroy({
        where: {
          [Op.or]: oldStatusItems
        },
        transaction
      })

      await database.status.destroy({
        where: {
          [Op.or]: oldStatusItems
        },
        transaction
      })

      return true
    })
  }

  if (await cleanupStatusRound()) {
    while (await cleanupStatusRound()) {
      // do nothing
    }
  }
}
