/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as Sequelize from 'sequelize'
import { SequelizeAttributes } from './types'

export interface MessageAttributesVersion1 {
  senderDeviceId: string
  messageId: string
  receiverDeviceId: string
  sequenceId: number
  content: string
  time: number
}

export type MessageAttributes = MessageAttributesVersion1

export type MessageModel = Sequelize.Model & MessageAttributes
export type MessageModelStatic = typeof Sequelize.Model & {
  new (values?: object, options?: Sequelize.BuildOptions): MessageModel
}

export const attributesVersion1: SequelizeAttributes<MessageAttributesVersion1> = {
  senderDeviceId: {
    primaryKey: true,
    type: Sequelize.STRING(24),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9+/=]{24}$/
    }
  },
  messageId: {
    primaryKey: true,
    type: Sequelize.STRING(10),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9]{10}$/
    }
  },
  receiverDeviceId: {
    type: Sequelize.STRING(24),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9+/=]{24}$/
    }
  },
  sequenceId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      min: 0
    }
  },
  content: {
    type: new Sequelize.TEXT('medium'),
    allowNull: false,
    validate: {
      is: /^[a-zA-Z0-9+/=]*$/
    }
  },
  time: {
    type: Sequelize.BIGINT,
    allowNull: false,
    validate: {
      min: 0
    }
  }
}

export const attributes: SequelizeAttributes<MessageAttributes> = {
  ...attributesVersion1
}

export const createMessageModel = (sequelize: Sequelize.Sequelize): MessageModelStatic => sequelize.define('Message', attributes) as MessageModelStatic
