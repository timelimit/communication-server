/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as Sequelize from 'sequelize'
import { createDeviceModel, DeviceModelStatic } from './device'
import { createMessageModel, MessageModelStatic } from './message'
import { createUmzug } from './migration/umzug'
import { createStatusModel, StatusModelStatic } from './status'
import { createStatusReceiverModel, StatusReceiverModelStatic } from './statusreceiver'

export interface Database {
  device: DeviceModelStatic
  message: MessageModelStatic
  status: StatusModelStatic
  statusReceiver: StatusReceiverModelStatic
  transaction: <T> (autoCallback: (t: Sequelize.Transaction) => Promise<T>) => Promise<T>
}

const createDatabase = (sequelize: Sequelize.Sequelize): Database => ({
  device: createDeviceModel(sequelize),
  message: createMessageModel(sequelize),
  status: createStatusModel(sequelize),
  statusReceiver: createStatusReceiverModel(sequelize),
  transaction: <T> (autoCallback: (transaction: Sequelize.Transaction) => Promise<T>) => (sequelize.transaction({
    isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_COMMITTED
  }, autoCallback) as any) as Promise<T>
})

export const sequelize = new Sequelize.Sequelize(process.env.DATABASE_URL || 'sqlite://test.db', {
  define: {
    timestamps: false
  },
  logging: false
})

export const defaultDatabase = createDatabase(sequelize)
export const defaultUmzug = createUmzug(sequelize)
