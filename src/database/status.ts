/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as Sequelize from 'sequelize'
import { SequelizeAttributes } from './types'

export interface StatusAttributesVersion1 {
  senderDeviceId: string
  statusId: string
  content: string
  time: number
}

export type StatusAttributes = StatusAttributesVersion1

export type StatusModel = Sequelize.Model & StatusAttributes
export type StatusModelStatic = typeof Sequelize.Model & {
  new (values?: object, options?: Sequelize.BuildOptions): StatusModel
}

export const attributesVersion1: SequelizeAttributes<StatusAttributesVersion1> = {
  senderDeviceId: {
    primaryKey: true,
    type: Sequelize.STRING(24),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9+/=]{24}$/
    }
  },
  statusId: {
    primaryKey: true,
    type: Sequelize.STRING(10),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9]{10}$/
    }
  },
  content: {
    type: new Sequelize.TEXT('medium'),
    allowNull: false,
    validate: {
      is: /^[a-zA-Z0-9+/=]*$/
    }
  },
  time: {
    type: Sequelize.BIGINT,
    allowNull: false,
    validate: {
      min: 0
    }
  }
}

export const attributes: SequelizeAttributes<StatusAttributes> = {
  ...attributesVersion1
}

export const createStatusModel = (sequelize: Sequelize.Sequelize): StatusModelStatic => sequelize.define('Status', attributes) as StatusModelStatic
