/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as Sequelize from 'sequelize'
import { SequelizeAttributes } from './types'

export interface DeviceAttributesVersion1 {
  deviceId: string
  authTokenHash: string
  lastSeen: string
  nextSequenceNumber: number
}

export type DeviceAttributes = DeviceAttributesVersion1

export type DeviceModel = Sequelize.Model & DeviceAttributes
export type DeviceModelStatic = typeof Sequelize.Model & {
  new (values?: object, options?: Sequelize.BuildOptions): DeviceModel
}

export const attributesVersion1: SequelizeAttributes<DeviceAttributesVersion1> = {
  deviceId: {
    primaryKey: true,
    type: Sequelize.STRING(24),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9+/=]{24}$/
    }
  },
  authTokenHash: {
    type: Sequelize.STRING(128),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[0-9a-f]{128}$/
    }
  },
  lastSeen: {
    type: Sequelize.BIGINT,
    allowNull: false,
    validate: {
      min: 0
    }
  },
  nextSequenceNumber: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      min: 0
    }
  }
}

export const attributes: SequelizeAttributes<DeviceAttributes> = {
  ...attributesVersion1
}

export const createDeviceModel = (sequelize: Sequelize.Sequelize): DeviceModelStatic => sequelize.define('Device', attributes) as DeviceModelStatic
