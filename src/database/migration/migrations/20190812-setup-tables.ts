/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { QueryInterface, Sequelize, Transaction } from 'sequelize'
import { attributes as deviceAttributes } from '../../device'
import { attributes as messageAttributes } from '../../message'
import { attributes as statusAttributes } from '../../status'
import { attributes as statusReceiverAttributes } from '../../statusreceiver'

export async function up (queryInterface: QueryInterface, sequelize: Sequelize) {
  await sequelize.transaction({
    type: Transaction.TYPES.EXCLUSIVE
  }, async (transaction) => {
    await queryInterface.createTable('Devices', deviceAttributes, { transaction })
    await queryInterface.createTable('Messages', messageAttributes, { transaction })
    await queryInterface.createTable('Statuses', statusAttributes, { transaction })
    await queryInterface.createTable('StatusReceivers', statusReceiverAttributes, { transaction })

    await queryInterface.addIndex('Devices', ['lastSeen'], { transaction })
    await queryInterface.addIndex('Messages', ['receiverDeviceId'], { transaction })
    await queryInterface.addIndex('Messages', ['time'], { transaction })
    await queryInterface.addIndex('StatusReceivers', ['receiverDeviceId'], { transaction })

    await queryInterface.addConstraint('Statuses', ['senderDeviceId'], {
      type: 'foreign key',
      references: {
        table: 'Devices',
        field: 'deviceId'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      transaction
    })

    await queryInterface.addConstraint('Messages', ['senderDeviceId'], {
      type: 'foreign key',
      references: {
        table: 'Devices',
        field: 'deviceId'
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      transaction
    })
  })
}

export async function down () {
  throw new Error('not possible')
}
