/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as Sequelize from 'sequelize'
import { SequelizeAttributes } from './types'

export interface StatusReceiverAttributesVersion1 {
  senderDeviceId: string
  statusId: string
  receiverDeviceId: string
}

export type StatusReceiverAttributes = StatusReceiverAttributesVersion1

export type StatusReceiverModel = Sequelize.Model & StatusReceiverAttributes
export type StatusReceiverModelStatic = typeof Sequelize.Model & {
  new (values?: object, options?: Sequelize.BuildOptions): StatusReceiverModel
}

export const attributesVersion1: SequelizeAttributes<StatusReceiverAttributesVersion1> = {
  senderDeviceId: {
    primaryKey: true,
    type: Sequelize.STRING(24),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9+/=]{24}$/
    }
  },
  statusId: {
    primaryKey: true,
    type: Sequelize.STRING(10),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9]{10}$/
    }
  },
  receiverDeviceId: {
    primaryKey: true,
    type: Sequelize.STRING(24),
    allowNull: false,
    validate: {
      notEmpty: true,
      is: /^[a-zA-Z0-9+/=]{24}$/
    }
  }
}

export const attributes: SequelizeAttributes<StatusReceiverAttributes> = {
  ...attributesVersion1
}

export const createStatusReceiverModel = (sequelize: Sequelize.Sequelize): StatusReceiverModelStatic => sequelize.define('StatusReceiver', attributes) as StatusReceiverModelStatic
