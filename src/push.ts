/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { uniq } from 'lodash'
import * as Sequelize from 'sequelize'
import { Database } from './database'
import { ChangeNotification, ChangeNotifications, ExecutedChanges } from './schema'

export async function getChangeNotifications ({ changes, database, transaction }: {
  changes: ExecutedChanges
  database: Database
  transaction: Sequelize.Transaction
}): Promise<ChangeNotifications> {
  const allDeviceIds = uniq([
    ...changes.devicesWithNewMessages,
    ...changes.devicesWithNewStatusContent,
    ...changes.devicesWithRemovedSentMessages
  ])

  if (allDeviceIds.length === 0) {
    return []
  }

  const result: Array<ChangeNotification> = []

  const deviceEntries = await database.device.findAll({
    where: {
      deviceId: {
        [Sequelize.Op.in]: allDeviceIds
      }
    },
    attributes: ['deviceId', 'authTokenHash'],
    transaction
  }).map((item) => ({
    deviceId: item.deviceId,
    authTokenHash: item.authTokenHash
  }))

  deviceEntries.forEach(({ deviceId, authTokenHash }) => {
    result.push({
      deviceIdAndAuthTokenHash: deviceId + authTokenHash,
      content: {
        messages: changes.devicesWithNewMessages.indexOf(deviceId) !== -1,
        status: changes.devicesWithNewStatusContent.indexOf(deviceId) !== -1,
        deliveredMessages: changes.devicesWithRemovedSentMessages.indexOf(deviceId) !== -1
      }
    })
  })

  return result
}
