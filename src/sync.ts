/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { createHash } from 'crypto'
import { BadRequest, Forbidden, InternalServerError } from 'http-errors'
import { difference, groupBy, uniq } from 'lodash'
import * as Sequelize from 'sequelize'
import { Database } from './database'
import { getChangeNotifications } from './push'
import { ChangeNotifications, ExecutedChanges, SyncRequest, SyncResponse } from './schema'
import { generateMessageId } from './token'
import { validateRequest } from './validator'

export function hashAuthToken (authToken: string) {
  if (authToken.length < 32) {
    throw new BadRequest()
  }

  return createHash('sha512').update(authToken).digest('hex')
}

export async function handleSyncRequest ({ request, database }: {
  request: SyncRequest
  database: Database
}): Promise<{
  response: SyncResponse
  changes: ChangeNotifications
}> {
  if (!validateRequest(request)) {
    throw new BadRequest()
  }

  const authTokenHash = hashAuthToken(request.authToken)
  const now = Date.now()

  return database.transaction(async (transaction) => {
    // this is deduplicated later
    const changes: ExecutedChanges = {
      devicesWithNewMessages: [],
      devicesWithNewStatusContent: [],
      devicesWithRemovedSentMessages: []
    }

    // check authentication
    {
      const deviceEntry = await database.device.findOne({
        where: {
          deviceId: request.deviceId
        },
        transaction
      })

      if (deviceEntry) {
        if (deviceEntry.authTokenHash !== authTokenHash) {
          throw new Forbidden()
        }

        await database.device.update({
          lastSeen: now.toString(10)
        }, {
          where: {
            deviceId: request.deviceId
          },
          transaction
        })
      } else {
        await database.device.create({
          deviceId: request.deviceId,
          authTokenHash,
          lastSeen: now.toString(10),
          nextSequenceNumber: 0
        }, { transaction })
      }
    }

    // handle send messages
    if (request.sendMessages.length > 0) {
      const oldNextSequenceNumber: number = await (async () => {
        const deviceEntry = await database.device.findOne({
          where: {
            deviceId: request.deviceId
          },
          transaction
        })

        if (!deviceEntry) {
          throw new InternalServerError()
        }

        return deviceEntry.nextSequenceNumber
      })()

      let nextSequenceNumber = oldNextSequenceNumber

      for (let i = 0; i < request.sendMessages.length; i++) {
        const messageToSend = request.sendMessages[i]

        if (messageToSend.sequenceId < nextSequenceNumber) {
          continue
        }

        await database.message.create({
          senderDeviceId: request.deviceId,
          messageId: generateMessageId(),
          receiverDeviceId: messageToSend.receiverDeviceId,
          sequenceId: messageToSend.sequenceId,
          content: messageToSend.content,
          time: now.toString(10)
        }, { transaction })

        changes.devicesWithNewMessages.push(messageToSend.receiverDeviceId)

        nextSequenceNumber = messageToSend.sequenceId + 1
      }

      if (nextSequenceNumber !== oldNextSequenceNumber) {
        await database.device.update({
          nextSequenceNumber
        }, {
          where: {
            deviceId: request.deviceId
          },
          transaction
        })
      }
    }

    const pendingSentMessageSequenceIds = await database.message.findAll({
      where: {
        senderDeviceId: request.deviceId
      },
      attributes: ['sequenceId'],
      transaction
    }).map((item) => item.sequenceId)

    // handle put status
    const missingStatusContent: Array<string> = []

    {
      const currentStatusMessageIds = await database.status.findAll({
        where: {
          senderDeviceId: request.deviceId
        },
        attributes: ['statusId'],
        transaction
      }).map((item) => item.statusId)

      const currentStatusReceivers = await database.statusReceiver.findAll({
        where: {
          senderDeviceId: request.deviceId
        },
        attributes: ['statusId', 'receiverDeviceId'],
        transaction
      }).map((item) => ({
        statusId: item.statusId,
        receiverDeviceId: item.receiverDeviceId
      }))

      const currentStatusReceiversByStatusId = groupBy(currentStatusReceivers, (item) => item.statusId)

      const updatedStatusesIds: Array<string> = []

      // process new/ updated status items
      for (let i = 0; i < request.putStatus.length; i++) {
        const putStatusItem = request.putStatus[i]

        updatedStatusesIds.push(putStatusItem.statusId)

        if (currentStatusMessageIds.indexOf(putStatusItem.statusId) === -1) {
          // new item

          if (putStatusItem.newContent !== undefined) {
            // got content => insert it
            await database.status.create({
              senderDeviceId: request.deviceId,
              statusId: putStatusItem.statusId,
              content: putStatusItem.newContent,
              time: now.toString(10)
            }, { transaction })

            await database.statusReceiver.bulkCreate(
              putStatusItem.receiverDeviceIds.map((receiverDeviceId) => ({
                senderDeviceId: request.deviceId,
                statusId: putStatusItem.statusId,
                receiverDeviceId
              })),
              { transaction }
            )

            putStatusItem.receiverDeviceIds.forEach((receiver) => changes.devicesWithNewStatusContent.push(receiver))
          } else {
            // content missing => inform sender
            missingStatusContent.push(putStatusItem.statusId)
          }
        } else {
          if (putStatusItem.newContent !== undefined) {
            // update item

            await database.status.update({
              content: putStatusItem.newContent,
              time: now.toString(10)
            }, {
              where: {
                senderDeviceId: request.deviceId,
                statusId: putStatusItem.statusId
              },
              transaction
            })

            await database.statusReceiver.destroy({
              where: {
                senderDeviceId: request.deviceId,
                statusId: putStatusItem.statusId
              },
              transaction
            })

            await database.statusReceiver.bulkCreate(putStatusItem.receiverDeviceIds.map((receiverDeviceId) => ({
              senderDeviceId: request.deviceId,
              statusId: putStatusItem.statusId,
              receiverDeviceId
            })), { transaction })

            putStatusItem.receiverDeviceIds.forEach((receiver) => changes.devicesWithNewStatusContent.push(receiver))
          } else {
            // keep item

            await database.status.update({
              time: now.toString(10)
            }, {
              where: {
                senderDeviceId: request.deviceId,
                statusId: putStatusItem.statusId
              },
              transaction
            })

            const currentReceiversUnsafe = currentStatusReceiversByStatusId[putStatusItem.statusId]
            const currentReceivers = (currentReceiversUnsafe ? currentReceiversUnsafe : []).map((item) => item.receiverDeviceId)

            const removedReceivers = difference(currentReceivers, putStatusItem.receiverDeviceIds)
            const addedReceivers = difference(putStatusItem.receiverDeviceIds, currentReceivers)
            const changedReceivers = [...removedReceivers, ...addedReceivers]

            if (changedReceivers.length > 0) {
              await database.statusReceiver.destroy({
                where: {
                  senderDeviceId: request.deviceId,
                  statusId: putStatusItem.statusId
                },
                transaction
              })

              await database.statusReceiver.bulkCreate(putStatusItem.receiverDeviceIds.map((receiverDeviceId) => ({
                senderDeviceId: request.deviceId,
                statusId: putStatusItem.statusId,
                receiverDeviceId
              })), { transaction })
            }

            changedReceivers.forEach((receiver) => changes.devicesWithNewStatusContent.push(receiver))
          }
        }
      }

      // remove old items
      const oldMessageIds = difference(currentStatusMessageIds, updatedStatusesIds)

      if (oldMessageIds.length > 0) {
        await database.status.destroy({
          where: {
            senderDeviceId: request.deviceId,
            statusId: {
              [Sequelize.Op.in]: oldMessageIds
            }
          },
          transaction
        })

        const oldStatusReceivers = await database.statusReceiver.findAll({
          where: {
            senderDeviceId: request.deviceId,
            statusId: {
              [Sequelize.Op.in]: oldMessageIds
            }
          },
          attributes: ['receiverDeviceId'],
          transaction
        }).map((item) => item.receiverDeviceId)

        oldStatusReceivers.forEach((receiver) => changes.devicesWithNewStatusContent.push(receiver))

        await database.statusReceiver.destroy({
          where: {
            senderDeviceId: request.deviceId,
            statusId: {
              [Sequelize.Op.in]: oldMessageIds
            }
          },
          transaction
        })
      }
    }

    // get status updates
    const statusUpdates: Array<{
      statusId: string
      senderDeviceId: string
      content?: string
      time: number
    }> = []

    {
      const statusUpdateReceiverItems = await database.statusReceiver.findAll({
        where: {
          receiverDeviceId: request.deviceId
        },
        attributes: ['senderDeviceId', 'statusId'],
        transaction
      }).map((item) => ({
        senderDeviceId: item.senderDeviceId,
        statusId: item.statusId
      }))

      const statusUpdateItems = await database.status.findAll({
        where: {
          [Sequelize.Op.or]: statusUpdateReceiverItems
        },
        transaction
      })

      for (let i = 0; i < statusUpdateItems.length; i++) {
        const statusUpdateItem = statusUpdateItems[i]
        const contentHash = createHash('sha512').update(statusUpdateItem.content).digest('hex')
        const clientEntry = request.receivedStatus.find((item) => item.senderDeviceId === statusUpdateItem.senderDeviceId && item.statusId === statusUpdateItem.statusId)

        if ((!clientEntry) || clientEntry.contentHash !== contentHash) {
          statusUpdates.push({
            statusId: statusUpdateItem.statusId,
            senderDeviceId: statusUpdateItem.senderDeviceId,
            content: statusUpdateItem.content,
            time: statusUpdateItem.time
          })
        }
      }

      for (let i = 0; i < request.receivedStatus.length; i++) {
        const receivedStatusItem = request.receivedStatus[i]

        if (!statusUpdateItems.find((item) => item.senderDeviceId === receivedStatusItem.statusId && item.statusId === receivedStatusItem.senderDeviceId)) {
          statusUpdates.push({
            statusId: receivedStatusItem.statusId,
            senderDeviceId: receivedStatusItem.senderDeviceId,
            time: now
          })
        }
      }
    }

    // process received message ids
    if (request.receivedMessageIds.length > 0) {
      const removedMessageSenders = await database.message.findAll({
        where: {
          receiverDeviceId: request.deviceId,
          messageId: {
            [Sequelize.Op.in]: request.receivedMessageIds
          }
        },
        attributes: ['senderDeviceId'],
        transaction
      }).map((item) => item.senderDeviceId)

      removedMessageSenders.forEach((deviceId) => changes.devicesWithRemovedSentMessages.push(deviceId))

      await database.message.destroy({
        where: {
          receiverDeviceId: request.deviceId,
          messageId: {
            [Sequelize.Op.in]: request.receivedMessageIds
          }
        },
        transaction
      })
    }

    // get new messages
    const newMessages = await database.message.findAll({
      where: {
        receiverDeviceId: request.deviceId
      },
      attributes: ['senderDeviceId', 'messageId', 'content'],
      transaction
    }).map((item) => ({
      messageId: item.messageId,
      senderDeviceId: item.senderDeviceId,
      content: item.content
    }))

    const response: SyncResponse = {
      missingStatusContent,
      newMessages,
      statusUpdates,
      time: now,
      pendingSentMessageSequenceIds
    }

    const deduplicatedChanges = {
      devicesWithNewMessages: uniq(changes.devicesWithNewMessages),
      devicesWithNewStatusContent: uniq(changes.devicesWithNewStatusContent),
      devicesWithRemovedSentMessages: uniq(changes.devicesWithRemovedSentMessages)
    }

    const changeNotifications = await getChangeNotifications({
      changes: deduplicatedChanges,
      database,
      transaction
    })

    return {
      response,
      changes: changeNotifications
    }
  })
}
