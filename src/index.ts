/*
 * Communication Server - a silly backend which saves and delivers messages
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { createServer } from './api'
import { cleanupDatabase } from './clear'
import { defaultDatabase, defaultUmzug } from './database'
import { sleep } from './sleep'

(async () => {
  await defaultUmzug.up()
  await cleanupDatabase(defaultDatabase)

  createServer({ database: defaultDatabase }).listen(process.env.PORT || 8080)

  ;(async () => {
    while (true) {
      await sleep(1000 * 60 * 60 /* 1 hour */)

      try {
        await cleanupDatabase(defaultDatabase)
      } catch (ex) {
        console.warn('database cleanup failed', ex)
      }
    }
  })().catch((ex) => {
    // this should never happen

    console.warn(ex)
    process.exit(1)
  })

  console.log('ready')
})().catch((ex) => {
  console.warn(ex)
  process.exit(1)
})
